package json;

import com.fasterxml.jackson.databind.ObjectMapper;
import model.Plane;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;



public class JSONParser {
	private ObjectMapper objectMapper;
	private static final Logger LOGGER = LogManager.getLogger(JSONParser.class);

	public JSONParser() {
		this.objectMapper = new ObjectMapper();
	}

	public List<Plane> getPlaneList(File json) {
		FilePath filePath = new FilePath();
		Plane[] planes = new Plane[0];
		try {
			planes = objectMapper.readValue(new File(filePath.propertyFile("json")), Plane[].class);
		} catch (IOException e) {
			LOGGER.error("FILE_NOT_FOUND_EXC" + e.getClass());
			e.printStackTrace();
		}
		return Arrays.asList(planes);
	}
}


