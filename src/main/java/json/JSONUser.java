package json;

import model.Plane;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.List;

import static com.sun.xml.internal.ws.spi.db.BindingContextFactory.LOGGER;

public class JSONUser {
     private static final Logger LOGGER = LogManager.getLogger(JSONUser.class);

  public void showСontents(){
    File json = new File("src/main/resources/json/planeJSON.json");
    File schema = new File("src/main/resources/json/plainJSONScheme.json");

    JSONParser parser = new JSONParser();
    printList(parser.getPlaneList(json));
  }

  private static void printList(List<Plane> planes) {
    LOGGER.info("JSON");
    for (Plane plane : planes) {
      System.out.println(plane);
    }
  }
}
