package model;

public class Parameters {
	private double length;
	private double weight;
	private double height;

	public Parameters() {
	}

	public Parameters(double length, double weight, double height) {
		this.length = length;
		this.weight = weight;
		this.height = height;
	}

	public double getLength() {
		return length;
	}

	public void setLength(double length) {
		this.length = length;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public String toString() {
		return "Parameters{" +
				" lenght ='" + length + '\'' +
				", weight ='" + weight + '\'' +
				", height =" + height + '\'' +
				'}';
	}
}
