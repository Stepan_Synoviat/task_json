package model;

public class Chars {
	private String type;
	private int seats;
	private boolean kit;
	private int rockets;
	private Parameters parameters;
	private int price;
	private boolean Radar;

	public Chars(){
	}

	public Chars(String type, int seats, boolean kit, int rockets, Parameters parameters, int price, boolean hasRadar) {
		this.type = type;
		this.seats = seats;
		this.kit = kit;
		this.rockets = rockets;
		this.parameters = parameters;
		this.price = price;
		this.Radar = hasRadar;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getSeats() {
		return seats;
	}

	public void setSeats(int seats) {
		this.seats = seats;
	}

	public boolean getKit() {
		return kit;
	}

	public void setKit(boolean kit) {
		this.kit = kit;
	}

	public int getRockets() {
		return rockets;
	}

	public void setRockets(int rockets) {
		this.rockets = rockets;
	}

	public Parameters getParameters() {
		return parameters;
	}

	public void setParameters(Parameters parameters) {
		this.parameters = parameters;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public boolean isHasRadar() {
		return Radar;
	}

	public void setHasRadar(boolean hasRadar) {
		this.Radar = hasRadar;
	}

	public String toString() {
		return "Chars{" +
				" type ='" + type + '\'' +
				", seats ='" + seats + '\'' +
				", kit =" + kit + '\'' +
				", rockets ='" + rockets + '\'' +
				", price ='" + price + '\'' +
				'}';
	}
}
