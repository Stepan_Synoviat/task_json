package model;

public class Plane {
	private String model;
	private String origin;
	private Chars chars;

	public Plane(){
	}

	public Plane(String model, String origin, Chars chars) {
		this.model = model;
		this.origin = origin;
		this.chars = chars;
	}


	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public Chars getChars() {
		return chars;
	}

	public void setChars(Chars chars) {
		this.chars = chars;
	}

	@Override
	public String toString() {
		return "Plane{" +
				" model='" + model + '\'' +
				", origin='" + origin + '\'' +
				", chars=" + chars +
				'}';
	}
}
